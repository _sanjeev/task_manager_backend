const express = require("express");
const { createTask, getAllTask, getTask, updateTask, deleteTask } = require("../controllers/task-controller");
const router = express.Router();

router.post("/task", createTask);
router.get("/tasks", getAllTask);
router.get("/task/:id", getTask);
router.patch("/task/:id", updateTask);
router.delete("/task/:id", deleteTask);

module.exports = router;
