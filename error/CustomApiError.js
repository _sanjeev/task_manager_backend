class CustomApiError extends Error {
    constructor(message, statusCode, status, entity) {
        super(message);
        this.statusCode = statusCode;
        this.status = status;
        this.entity = entity;
    }
}

const customApiError = (message, statusCode, status, entity) => {
    return new CustomApiError(message, statusCode, status, entity);
};

module.exports = {
    customApiError,
    CustomApiError,
};
