const express = require("express");
const app = express();
const dotenv = require("dotenv");
const connectDB = require("./db/connect");
const { notFound } = require("./middleware/not-found");
const { errorHandler } = require("./middleware/error-handler");
dotenv.config();

app.use(express.json());
app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const PORT = 3000;

const start = async () => {
    await connectDB(process.env.MONGO_URI);
    app.listen(PORT, (err) => {
        if (err) {
            console.log("Error : ", err);
            return;
        }
        console.log(`Server is listening on port : ${PORT}`);
    });
};

start();
