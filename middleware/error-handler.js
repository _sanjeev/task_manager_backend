const { CustomApiError } = require("../error/CustomApiError");

module.exports.errorHandler = (err, req, res, next) => {
    if (err instanceof CustomApiError) {
        return res.status(err.statusCode).json({
            status: err.status,
            message: err.message,
            entity: err.entity,
        });
    }
    return res.status(500).json({
        status: false,
        message: err,
    });
};
