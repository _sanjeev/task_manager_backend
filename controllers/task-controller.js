const { customApiError } = require("../error/CustomApiError");
const { asyncWrapper } = require("../middleware/async-wrapper");
const Task = require("../models/Task");

module.exports.createTask = asyncWrapper(async (req, res) => {
    const { name, completed } = req.body;
    const task = await Task.create({ name: name, completed: completed });
    return res.status(201).json({
        status: true,
        message: "Successfully created new task!",
        entity: task,
    });
});

module.exports.getAllTask = asyncWrapper(async (req, res) => {
    const allTask = await Task.find({});
    return res.status(200).json({
        status: true,
        message: "Successfully get all the task!",
        entity: allTask,
    });
});

module.exports.getTask = asyncWrapper(async (req, res, next) => {
    const { id: taskId } = req.params;
    const task = await Task.findOne({
        _id: taskId,
    });

    if (!task) {
        return next(customApiError("Failed to get the resources", 404, false, null));
    }

    return res.status(200).json({
        status: true,
        message: "Successfully get the task!",
        entity: task,
    });
});

module.exports.updateTask = asyncWrapper(async (req, res) => {
    const { id: taskId } = req.params;
    const task = await Task.findOneAndUpdate(
        {
            _id: taskId,
        },
        req.body,
        {
            new: true,
            runValidators: true,
        }
    );
    return res.status(200).json({
        status: true,
        message: "Successfully update the task!",
        entity: task,
    });
});

module.exports.deleteTask = asyncWrapper(async (req, res) => {
    const { id: taskId } = req.params;
    const task = await Task.findOneAndDelete({
        _id: taskId,
    });
    return res.status(200).json({
        status: true,
        message: "Successfully delete the task!",
        entity: task,
    });
});
